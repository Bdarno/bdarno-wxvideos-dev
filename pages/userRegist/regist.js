const app = getApp()

Page({
    data: {

    },
    doRegist:function(e){
      var formObject = e.detail.value;
      var username = formObject.username;
      var password = formObject.password;

      // 简单验证
      if(username.length == 0 || password.length==0){
        wx.showToast({
          title: '用户名或密码不能为空',
          icon:'none',
          duration:3000
        })
      }else{
        var serverUrl =app.serverUrl;
        wx.showLoading({
          title: '请等待...',
        })
        wx.request({
          url: serverUrl+'/regist',
          method:'POST',
          data:{
            username: username,
            password: password
          },
          header: {
            'content-type': 'application/json' // 默认值
          },
          success:function(result){
            wx.hideLoading();
            console.log(result.data);
            var status =result.data.status;
            var msg =result.data.msg;
            if(status ==200){
              wx.showToast({
                title: "用户注册成功！！！",
                icon: "sucess",
                duration: 3000
              });
              // app.userInfo = result.data.data;
              app.setGlobalUserInfo(result.data.data);

            }else if(status ==500){
                wx.showToast({
                  title: msg,
                  icon:'none',
                  duration:3000
                })
            }
          }
        })
      }
    },
    goLoginPage(){
      wx.redirectTo({
        url: '../userLogin/login',
      })
    }
})