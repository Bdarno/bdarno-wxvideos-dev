
const app = getApp()

Page({
    data: {
      bgmList:[],
      serverUrl:'',
      videoParams:{}
    },

    onLoad: function (params) {
      var serverUrl=app.serverUrl;
      var that =this;
      var user = app.getGlobalUserInfo();
      that.setData({
        videoParams:params
      });

      wx.showLoading({
        title: '请等待...',
      })
      wx.request({
        url: serverUrl + '/bgm/list',
        method: "POST",
        header: {
          'content-type': 'application/json', // 默认值
          'headerUserId': user.uid,
          'headerUserToken': user.userToken
        },
        success: function (result) {
          console.log(result);
          var status = result.data.status;
          var msg = result.data.msg;
         
          wx.hideLoading();
          if (status == 200) {
            var bgmList = result.data.data;
           
            that.setData({
              bgmList :bgmList,
              serverUrl: serverUrl
            });
          } else if (result.data.status == 502) {
            wx.showToast({
              title: result.data.msg,
              duration: 2000,
              icon: "none",
              success: function () {
                wx.redirectTo({
                  url: '../userLogin/login',
                })
              }
            });
          }
        }
      })
    },
    upload:function(e){
      var that = this;
      var bgmId =e.detail.value.bgmId;
      var desc = e.detail.value.desc;
      console.log(bgmId+":"+desc);

      var duration = that.data.videoParams.duration;
      var tmpWidth = that.data.videoParams.tmpWidth;
      var tmpHeight = that.data.videoParams.tmpHeight;
      var tmpVideoUrl = that.data.videoParams.tmpVideoUrl;
      var tmpCoverUrl = that.data.videoParams.tmpCoverUrl;

      //上传视频
      const serverUrl = app.serverUrl;
      var userInfo = app.getGlobalUserInfo();
      wx.showLoading({
        title: '上传中...',
      })
      wx.uploadFile({
        
        url: serverUrl + '/video/upload' ,
        formData:{
          userId: userInfo.uid,
          bgmId: bgmId,
          videoSeconds:duration,
          desc:desc,
          videoWidth: tmpWidth,
          videoHeight: tmpHeight
        },
        filePath: tmpVideoUrl,
        header: {
          'content-type': 'application/json', // 默认值
          'headerUserId': userInfo.uid,
          'headerUserToken': userInfo.userToken
        },
        name: 'file',
        success(res) {
          var data =JSON.parse(res.data);
          var status = data.status;
          var msg = data.msg;
          // do something
          wx.hideLoading();
      
          if (status == 200) {
            wx.showToast({
              title: '上传成功！~~',
              icon: "success"
            });
            wx.navigateBack({
              delta: 1,
            })
              // var videoId =data.data;
              // wx.showLoading({
              //   title: '上传中...',
              // })
              // wx.uploadFile({
              //   url: serverUrl + '/video/uploadCover',
              //   formData: {
              //     userId: app.userInfo.uid,
              //     videoId: videoId
              //   },
              //   filePath: tmpCoverUrl,
              //   header: {
              //     'content-type': 'application/json' // 默认值
              //   },
              //   name:'file',
              //   success:function(res){
              //     var data = JSON.parse(res.data);
              //     wx.hideLoading();
  
              //     if (data.status == 200) {
              //       wx.showToast({
              //         title: '上传成功！~~',
              //         icon:'success'
              //       });
              //       wx.navigateBack({
              //         delta: 1,
              //       })
              //     }else{
              //       wx.showToast({
              //         title: '上传失败！~~',
              //         icon: 'none'
              //       });
              //     }
              //   }
              // });
          } else if (res.data.status == 502) {
            wx.showToast({
              title: res.data.msg,
              duration: 2000,
              icon: "none"
            });
            wx.redirectTo({
              url: '../userLogin/login',
            })
          } else{
            wx.showToast({
              title: '上传失败！~~',
              icon:"none"
            })
          }

        }
      })
    }
})

