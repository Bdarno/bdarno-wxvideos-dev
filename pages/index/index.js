const app = getApp()

Page({
  data: {
    //  用于分页的属性
    screenWidth: 350,
    totalPage:1,
    page:1,
    videoList:[],
    serverUrl:"",
    searchContent:""
    
  },

  onLoad: function (params) {
    var that = this;
    var screenWidth = wx.getSystemInfoSync().screenWidth;
    that.setData({
      screenWidth: screenWidth,
    });
    var searchContent =params.search;
    var isRecord =params.isRecord;
    if(isRecord == null || isRecord =='' || isRecord == undefined){
      isRecord =0;
    }
    if (searchContent == null || searchContent == '' || searchContent == undefined){
      searchContent="";
    }
    that.setData({
      searchContent:searchContent
    });
    //获取当前的分页数
    var page = that.data.page;
    that.getAllVideoList(page,isRecord);
  },
  getAllVideoList: function (page, isRecord){
    var that = this;
    var serverUrl = app.serverUrl;
    wx.showLoading({
      title: '请等待，加载中...',
    });

    var searchContent = that.data.searchContent;
    wx.request({
      url: serverUrl + '/video/showAll?page=' + page+"&isRecord="+isRecord,
      method: "POST",
      data:{
        videoDesc:searchContent
      },
      success: function (res) {
        wx.hideLoading();
        wx.hideNavigationBarLoading();
        wx.stopPullDownRefresh();
        console.log(res);

        //判断当前页是否是第一页，如果是第一页，那么设置videoList为空
        if (page === 1) {
          that.setData({
            videoList: []
          });
        }
        var videoList = res.data.data.rows;
        var newVideoList = that.data.videoList;
        that.setData({
          videoList: newVideoList.concat(videoList),
          page: page,
          totalPage: res.data.data.total,
          serverUrl: serverUrl
        });


      }

    });
  },
  onReachBottom:function(){
    var that = this;
    var currentPage = that.data.page;
    var totalPage = that.data.totalPage;

    // 判断当前页数和总页数是否相等，如果相等则无需查询
    if (currentPage === totalPage){
      wx.showToast({
        title: '已经没有视频啦~~',
        icon:"none"
      });
      return;
    }
    var page =currentPage +1;
    that.getAllVideoList(page,0);
  },
  onPullDownRefresh:function(){
    wx.showNavigationBarLoading();
    this.getAllVideoList(1,0);
  },
  showVideoInfo:function(e){
    var that = this;
    var videoList = that.data.videoList;
    var arrindex = e.target.dataset.arrindex;
    //debugger;
    var videoInfo = JSON.stringify(videoList[arrindex]);
    wx.redirectTo({
      url: '../videoInfo/videoInfo?videoInfo=' + videoInfo,
    })
  }

})
