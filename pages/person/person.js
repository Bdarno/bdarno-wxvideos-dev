var videoUtils = require('../../utils/videoUtils.js')

const app = getApp()

Page({
  data: {
    faceUrl: "../resource/images/noneface.png",
    isMe: true,
    isFollow: false,


    videoSelClass: 'video-info',
    isSelectedWork: 'video-info-selected',
    isSelectedLike: "",
    isSelectedFollow: "",

    myVideoList: [],
    myVideoPage: 1,
    myVideoTotal: 1,

    likeVideoList: [],
    likeVideoPage: 1,
    likeVideoTotal: 1,

    followVideoList: [],
    followVideoPage: 1,
    followVideoTotal: 1,

    myWorkFalg: false,
    myLikesFalg: true,
    myFollowFalg: true

  },

  onLoad: function(params) {
    var that = this;
    var user = app.getGlobalUserInfo();
    var serverUrl = app.serverUrl;
    var publisherId = params.publisherId;
    var userId = user.uid;
    if (publisherId != null && publisherId != '' && publisherId != undefined) {
      userId = publisherId;
      that.setData({
        isMe: false,
        publisherId: publisherId
      });
    }

    wx.showLoading({
      title: '请等待...',
    })
    // 调用后端
    wx.request({
      url: serverUrl + '/user/queryUser?userId=' + userId + '&fanId=' + user.uid,
      method: "POST",
      header: {
        'content-type': 'application/json', // 默认值
        'headerUserId': user.uid,
        'headerUserToken': user.userToken
      },
      success: function(result) {
        console.log(result);
        var status = result.data.status;
        var msg = result.data.msg;
        wx.hideLoading();
        var faceUrl = "../resource/images/noneface.png";
        if (status == 200) {
          var me = result.data.data;
          if (me.buddha != null && me.buddha != '' &&
            me.buddha != undefined) {
            faceUrl = serverUrl + me.buddha;
          }
          that.setData({
            faceUrl: faceUrl,
            fansCounts: me.fansCounts,
            followCounts: me.followCounts,
            receiveLikeCounts: me.receiveLikeCounts,
            nickname: me.nickname,
            isFollow: me.follow,
            userId: userId
          });
        } else if (status == 502) {
          wx.showToast({
            title: result.data.msg,
            duration: 3000,
            icon: "none",
            success: function() {
              wx.redirectTo({
                url: '../userLogin/login',
              })
            }
          })
        }

      }
    })
  },
  logout: function() {
    var user = app.getGlobalUserInfo();
    var serverUrl = app.serverUrl;
    wx.showLoading({
      title: '请等待...',
    });
    wx.request({
      url: serverUrl + '/logout?userId=' + user.uid,
      method: 'POST',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(result) {
        wx.hideLoading();
        console.log(result.data);
        var status = result.data.status;
        var msg = result.data.msg;
        if (status == 200) {
          wx.showToast({
            title: "注销成功！",
            icon: "success",
            duration: 2000
          });
          // 注销以后清空缓存
          //app.userInfo =null ;
          wx.removeStorageSync("userInfo");
          wx.navigateTo({
            url: '../userLogin/login',
          })
        }
      }
    })
  },
  changeFace: function() {
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album'],
      success(res) {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths;
        wx.showLoading({
          title: '上传中...',
        })
        const serverUrl = app.serverUrl;
        var userInfo = app.getGlobalUserInfo();
        wx.uploadFile({
          url: serverUrl + '/user/uploadBuddha?userId=' + userInfo.uid,
          filePath: tempFilePaths[0],
          header: {
            'content-type': 'application/json' // 默认值
          },
          name: 'file',
          success(res) {
            const data = JSON.parse(res.data);
            var status = data.status;
            var msg = data.msg;
            // do something
            wx.hideLoading();
            if (status == 200) {
              wx.showToast({
                title: '上传成功！~~',
                icon: "success"
              });
              var imageUrl = data.data;
              that.setData({
                faceUrl: serverUrl + imageUrl
              });
            } else if (status == 500) {
              wx.showToast({
                title: msg
              })
            }

          }
        })
      }
    });
  },
  uploadVideo: function() {
    //videoUtils.uploadVideo();
    var that = this;
    wx.chooseVideo({
      sourceType: ['album'],
      success(res) {
        var duration = res.duration;
        var tmpWidth = res.width;
        var tmpHeight = res.height;
        var tmpVideoUrl = res.tempFilePath;
        var tmpCoverUrl = res.thumbTempFilePath;
        if (duration > 31) {
          wx.showToast({
            title: '视频长度不能超过30秒...',
            icon: "none",
            duration: 2500
          });
        } else if (duration < 1) {
          wx.showToast({
            title: '视频长度太短，请上传超过1秒的视频...',
            icon: "none",
            duration: 2500
          });
        } else {
          //  打开选择BGM的页面
          wx.navigateTo({
            url: "../chooseBgm/chooseBgm?duration=" + duration +
              "&tmpWidth=" + tmpWidth +
              "&tmpHeight=" + tmpHeight +
              "&tmpVideoUrl=" + tmpVideoUrl +
              "&tmpCoverUrl=" + tmpCoverUrl
          });
        }


      }
    })
  },
  followMe: function(e) {
    var that = this;
    var serverUrl = app.serverUrl;
    var publisherId = that.data.publisherId;
    var user = app.getGlobalUserInfo();
    var userId = user.uid;
    var followType = e.currentTarget.dataset.followtype;
    // 1: 关注  0：取消关注
    var url = '';
    if (followType == '1') {
      url = '/user/beyourfans?userId=' + publisherId + '&fanId=' + userId;
    } else {
      url = '/user/dontbeyourfans?userId=' + publisherId + '&fanId=' + userId;
    }
    wx.showLoading();
    wx.request({
      url: serverUrl + url,
      method: 'POST',
      header: {
        'content-type': 'application/json', // 默认值
        'headerUserId': user.uid,
        'headerUserToken': user.userToken
      },
      success: function() {
        wx.hideLoading();
        if (followType == '1') {
          that.setData({
            isFollow: true,
            fansCounts: ++that.data.fansCounts
          });
        } else {
          that.setData({
            isFollow: false,
            fansCounts: --that.data.fansCounts
          });
        }
      }
    })
  },

  doSelectWork: function() {
    this.setData({
      isSelectedWork: "video-info-selected",
      isSelectedLike: "",
      isSelectedFollow: "",

      myWorkFalg: false,
      myLikesFalg: true,
      myFollowFalg: true,

      myVideoList: [],
      myVideoPage: 1,
      myVideoTotal: 1,

      likeVideoList: [],
      likeVideoPage: 1,
      likeVideoTotal: 1,

      followVideoList: [],
      followVideoPage: 1,
      followVideoTotal: 1
    });

    this.getMyVideoList(1);
  },

  doSelectLike: function() {
    this.setData({
      isSelectedWork: "",
      isSelectedLike: "video-info-selected",
      isSelectedFollow: "",

      myWorkFalg: true,
      myLikesFalg: false,
      myFollowFalg: true,

      myVideoList: [],
      myVideoPage: 1,
      myVideoTotal: 1,

      likeVideoList: [],
      likeVideoPage: 1,
      likeVideoTotal: 1,

      followVideoList: [],
      followVideoPage: 1,
      followVideoTotal: 1
    });

    this.getMyLikesList(1);
  },

  doSelectFollow: function() {
    this.setData({
      isSelectedWork: "",
      isSelectedLike: "",
      isSelectedFollow: "video-info-selected",

      myWorkFalg: true,
      myLikesFalg: true,
      myFollowFalg: false,

      myVideoList: [],
      myVideoPage: 1,
      myVideoTotal: 1,

      likeVideoList: [],
      likeVideoPage: 1,
      likeVideoTotal: 1,

      followVideoList: [],
      followVideoPage: 1,
      followVideoTotal: 1
    });

    this.getMyFollowList(1)
  },

  getMyVideoList: function(page) {
    var me = this;

    // 查询视频信息
    wx.showLoading();
    // 调用后端
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl + '/video/showAll/?page=' + page + '&pageSize=6',
      method: "POST",
      data: {
        userId: me.data.userId
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(res) {
        console.log(res.data);
        var myVideoList = res.data.data.rows;
        wx.hideLoading();

        var newVideoList = me.data.myVideoList;
        me.setData({
          myVideoPage: page,
          myVideoList: newVideoList.concat(myVideoList),
          myVideoTotal: res.data.data.total,
          serverUrl: app.serverUrl
        });
      }
    })
  },

  getMyLikesList: function(page) {
    var me = this;
    var userId = me.data.userId;

    // 查询视频信息
    wx.showLoading();
    // 调用后端
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl + '/video/showMyLike/?userId=' + userId + '&page=' + page + '&pageSize=6',
      method: "POST",
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(res) {
        console.log(res.data);
        var likeVideoList = res.data.data.rows;
        wx.hideLoading();

        var newVideoList = me.data.likeVideoList;
        me.setData({
          likeVideoPage: page,
          likeVideoList: newVideoList.concat(likeVideoList),
          likeVideoTotal: res.data.data.total,
          serverUrl: app.serverUrl
        });
      }
    })
  },

  getMyFollowList: function(page) {
    var me = this;
    var userId = me.data.userId;

    // 查询视频信息
    wx.showLoading();
    // 调用后端
    var serverUrl = app.serverUrl;
    wx.request({
      url: serverUrl + '/video/showMyFollow/?userId=' + userId + '&page=' + page + '&pageSize=6',
      method: "POST",
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(res) {
        console.log(res.data);
        var followVideoList = res.data.data.rows;
        wx.hideLoading();

        var newVideoList = me.data.followVideoList;
        me.setData({
          followVideoPage: page,
          followVideoList: newVideoList.concat(followVideoList),
          followVideoTotal: res.data.data.total,
          serverUrl: app.serverUrl
        });
      }
    })
  },

  // 点击跳转到视频详情页面
  showVideo: function(e) {

    console.log(e);

    var myWorkFalg = this.data.myWorkFalg;
    var myLikesFalg = this.data.myLikesFalg;
    var myFollowFalg = this.data.myFollowFalg;

    if (!myWorkFalg) {
      var videoList = this.data.myVideoList;
    } else if (!myLikesFalg) {
      var videoList = this.data.likeVideoList;
    } else if (!myFollowFalg) {
      var videoList = this.data.followVideoList;
    }

    var arrindex = e.target.dataset.arrindex;
    var videoInfo = JSON.stringify(videoList[arrindex]);

    wx.redirectTo({
      url: '../videoInfo/videoInfo?videoInfo=' + videoInfo
    })

  },

  // 到底部后触发加载
  onReachBottom: function() {
    var myWorkFalg = this.data.myWorkFalg;
    var myLikesFalg = this.data.myLikesFalg;
    var myFollowFalg = this.data.myFollowFalg;

    if (!myWorkFalg) {
      var currentPage = this.data.myVideoPage;
      var totalPage = this.data.myVideoTotal;
      // 获取总页数进行判断，如果当前页数和总页数相等，则不分页
      if (currentPage === totalPage) {
        wx.showToast({
          title: '已经没有视频啦...',
          icon: "none"
        });
        return;
      }
      var page = currentPage + 1;
      this.getMyVideoList(page);
    } else if (!myLikesFalg) {
      var currentPage = this.data.likeVideoPage;
      var totalPage = this.data.myLikesTotal;
      // 获取总页数进行判断，如果当前页数和总页数相等，则不分页
      if (currentPage === totalPage) {
        wx.showToast({
          title: '已经没有视频啦...',
          icon: "none"
        });
        return;
      }
      var page = currentPage + 1;
      this.getMyLikesList(page);
    } else if (!myFollowFalg) {
      var currentPage = this.data.followVideoPage;
      var totalPage = this.data.followVideoTotal;
      // 获取总页数进行判断，如果当前页数和总页数相等，则不分页
      if (currentPage === totalPage) {
        wx.showToast({
          title: '已经没有视频啦...',
          icon: "none"
        });
        return;
      }
      var page = currentPage + 1;
      this.getMyFollowList(page);
    }

  }


})