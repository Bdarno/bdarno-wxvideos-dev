var videoUtils = require('../../utils/videoUtils.js')

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    cover:"cover",
    videoId:'',
    src:'',
    videoInfo:{},
    userLikeVideo:false,
    placeholder:'说点什么...',
    commentsPage: 1,
    commentsTotalPage: 1,
    commentsList: [],
  },
  videoCtx:{},
  onLoad:function(params){
    var that =this;
    that.videoCtx =wx.createVideoContext("myVideo", that);
    var serverUrl =app.serverUrl;
    // 获取视频参数
    var videoInfo = JSON.parse(params.videoInfo);
    var height = videoInfo.videoHeight;
    var width = videoInfo.videoWidth;
    var cover ="cover";
    if(width >= height){
      cover ="";
    }
    that.setData({
      videoId: videoInfo.vid,
      src: app.serverUrl+videoInfo.videoPath,
      videoInfo: videoInfo,
      cover:cover
    });
    var user = app.getGlobalUserInfo();
    var userLoginId = "";
    if (user != null && user != undefined && user != ''){
      userLoginId =user.uid;
    }
    wx.request({
      url: serverUrl + '/user/queryPublisher?loginUserId='+userLoginId+
        '&videoId=' + videoInfo.vid +'&publishUserId='+videoInfo.userId
      ,
      method:'POST',
      success:function(res){
        var publisher =res.data.data.publisher;
        var userLikeVideo = res.data.data.userLikeVideo;
        that.setData({
          publisher: publisher,
          userLikeVideo: userLikeVideo ,
          serverUrl: serverUrl
        });
      }
    });

    that.getCommentsList(1);
  },
  onShow:function(){
    var that = this;
    that.videoCtx.play();
  },
  onHide:function(){
    var that = this;
    that.videoCtx.pause();
  },
  showSearch:function(){
    wx.navigateTo({
      url: '../searchVideo/searchVideo',
    })
  },
  upload:function(){
    var that = this;
    var user = app.getGlobalUserInfo();
    var videoInfo = JSON.stringify(that.data.videoInfo);
    var realUrl ='../videoInfo/videoInfo#videoInfo@'+videoInfo;


    if (user == null || user == undefined || user == "") {
      wx.navigateTo({
        url: '../userLogin/login?redirectUrl='+realUrl,
      })
    } else {
      videoUtils.uploadVideo();
    }
    
  },
  showIndex:function(){
    wx.redirectTo({
      url: '../index/index',
    })
  },
  showMine:function(){
    var user = app.getGlobalUserInfo();
    if(user == null || user==undefined ||user ==""){
      wx.navigateTo({
        url: '../userLogin/login',
      })
    }else{
      wx.navigateTo({
        url: '../person/person',
      })
    }
  },
  likeVideoOrNot:function(){
    var that =this;
    var videoInfo = that.data.videoInfo;
    var user = app.getGlobalUserInfo();
    if (user == null || user == undefined || user == "") {
      wx.navigateTo({
        url: '../userLogin/login',
      })
    } else {
      var userLikeVideo =that.data.userLikeVideo;
      var url ='/video/userLike?userId='+user.uid+
        '&videoId=' + videoInfo.vid +'&videoAuthorId='+videoInfo.userId;
        if(userLikeVideo){
          url = '/video/userUnlike?userId=' + user.uid +
            '&videoId=' + videoInfo.vid + '&videoAuthorId=' + videoInfo.userId;
        }

        var serverUrl =app.serverUrl;
        wx.showLoading({
          title: '...',
        });
        
        wx.request({
          url: serverUrl +url,
          method:'POST',
          header: {
            'content-type': 'application/json', // 默认值
            'headerUserId': user.uid,
            'headerUserToken': user.userToken
          },
          success: function(res){
            console.log(res);
            wx.hideLoading();
            that.setData({
              userLikeVideo: !userLikeVideo
            });
          }
        })
    }
  },
  showPublisher:function(){
    var that = this;
    var user = app.getGlobalUserInfo();
    var videoInfo = that.data.videoInfo;
    var realUrl = '../person/person#videoInfo@publisherId=' + videoInfo.userId;


    if (user == null || user == undefined || user == "") {
      wx.navigateTo({
        url: '../userLogin/login?redirectUrl=' + realUrl,
      })
    } else {
      wx.navigateTo({
        url: '../person/person?publisherId=' + videoInfo.userId,
      })
    }
  },
  shareMe:function(){
    var me = this;
    var user = app.getGlobalUserInfo();

    wx.showActionSheet({
      itemList: ['下载到本地', '举报用户', '分享到朋友圈', '分享到QQ空间', '分享到微博'],
      success: function (res) {
        console.log(res.tapIndex);
        if (res.tapIndex == 0) {
          // 下载
          wx.showLoading({
            title: '下载中...',
          })
          wx.downloadFile({
            url: app.serverUrl + me.data.videoInfo.videoPath,
            success: function (res) {
              // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
              if (res.statusCode === 200) {
                console.log(res.tempFilePath);

                wx.saveVideoToPhotosAlbum({
                  filePath: res.tempFilePath,
                  success: function (res) {
                    console.log(res.errMsg)
                    wx.hideLoading();
                  }
                })
              }
            }
          })
        } else if (res.tapIndex == 1) {
          // 举报
          var videoInfo = JSON.stringify(me.data.videoInfo);
          var realUrl = '../videoInfo/videoInfo#videoInfo@' + videoInfo;

          if (user == null || user == undefined || user == '') {
            wx.navigateTo({
              url: '../userLogin/login?redirectUrl=' + realUrl,
            })
          } else {
            var publishUserId = me.data.videoInfo.userId;
            var videoId = me.data.videoInfo.vid;
            var currentUserId = user.uid;
            wx.navigateTo({
              url: '../report/report?videoId=' + videoId + "&publishUserId=" + publishUserId
            })
          }
        } else {
          wx.showToast({
            title: '官方暂未开放...',
          })
        }
      }
    });
  },
  onShareAppMessage: function (res) {

    var that = this;
    var videoInfo = that.data.videoInfo;

    return {
      title: '短视频内容分享',
      path: "pages/videoInfo/videoInfo?videoInfo=" + JSON.stringify(videoInfo)
    }
  },
  leaveComment:function(){
    this.setData({
      commentFocus: true
    });
  },
  saveComment:function(e){
    var that = this;
    var content = e.detail.value;
    var user =app.getGlobalUserInfo();

    // 获取评论回复的fatherCommentId和toUserId
    var fatherCommentId = e.currentTarget.dataset.replyfathercommentid;
    var toUserId = e.currentTarget.dataset.replytouserid;

    var videoInfo = JSON.stringify(that.data.videoInfo);
    var realUrl = '../videoInfo/videoInfo#videoInfo@' + videoInfo;

    if (user == null || user == undefined || user == '') {
      wx.navigateTo({
        url: '../userLogin/login?redirectUrl=' + realUrl,
      })
    } else {
      wx.showLoading({
        title: '请稍后...',
      })
      wx.request({
        url: app.serverUrl + '/video/saveComment?fatherCommentId=' + fatherCommentId + "&toUserId=" + toUserId,
        method: 'POST',
        header: {
          'content-type': 'application/json', // 默认值
          'headerUserId': user.uid,
          'headerUserToken': user.userToken
        },
        data:{
          fromUserId:user.uid,
          videoId: that.data.videoInfo.vid,
          comment:content,
        },
        success:function(res){
          console.log(res);
          wx.hideLoading();
          that.setData({
            contentValue:'',
            commentsList:[]
          });

          that.getCommentsList(1);
        }
      })

    }
  },
  getCommentsList: function (page) {
    var that =this;
    var videoId =that.data.videoInfo.vid;

    wx.request({
      url: app.serverUrl + '/video/getVideoComments?videoId='+ videoId+"&page="+page+"&pageSize=5",
      method:'POST',
      success:function(res){
        console.log(res.data);

        var commentsList = res.data.data.rows;
        var newCommentsList = that.data.commentsList;
        that.setData({
          commentsList: newCommentsList.concat(commentsList),
          commentsPage: page,
          commentsTotalPage: res.data.data.total
        });
      }
    });
  },
  replyFocus:function(e){
    var fatherCommentId = e.currentTarget.dataset.fathercommentid;
    var toUserId = e.currentTarget.dataset.touserid;
    var toNickname = e.currentTarget.dataset.tonickname;

    this.setData({
      placeholder: "回复  @" + toNickname,
      replyFatherCommentId: fatherCommentId,
      replyToUserId: toUserId,
      commentFocus: true
    });
  },
  onReachBottom: function () {
    var that = this;
    var currentPage = that.data.commentsPage;
    var totalPage = that.data.commentsTotalPage;
    if (currentPage === totalPage) {
      return;
    }
    var page = currentPage + 1;
    that.getCommentsList(page);
  }
})