const app = getApp()

Page({
    data: {

    },
    onLoad:function(params){
      var that =this;
      var redirectUrl =params.redirectUrl;
      if (redirectUrl != null && redirectUrl != undefined && redirectUrl != '') {
        redirectUrl = redirectUrl.replace(/#/g, "?");
        redirectUrl = redirectUrl.replace(/@/g, "=");

        that.redirectUrl = redirectUrl;
      }
    },
    doLogin:function(e){
      var that =this;
      var formObject = e.detail.value;
      var username = formObject.username;
      var password = formObject.password;
      // 简单验证
      if(username.length == 0 || password.length==0){
        wx.showToast({
          title: '用户名或密码不能为空',
          icon:'none',
          duration:3000
        })
      }else{
        var serverUrl =app.serverUrl;
        wx.showLoading({
          title: '请等待...',
        })
        wx.request({
          url: serverUrl +'/login',
          method:'POST',
          data:{
            username: username,
            password: password
          },
          header: {
            'content-type': 'application/json' // 默认值
          },
          success:function(result){
            wx.hideLoading();
            console.log(result.data);
            var status =result.data.status;
            var msg =result.data.msg;
            if(status ==200){
              //登录成功
              wx.showToast({
                title: "登录成功！",
                icon: "success",
                duration: 3000
              });
              //app.userInfo = result.data.data;
              app.setGlobalUserInfo(result.data.data);
              
              // 页面跳转
              var redirectUrl = that.redirectUrl;
              if (redirectUrl != null && redirectUrl != undefined && redirectUrl!=''){
                wx.redirectTo({
                  url: redirectUrl,
                });
              }else{
              
                wx.redirectTo({
                  url: '../index/index',
                });
              }
            }else if(status ==500){
              //失败弹出框
                wx.showToast({
                  title: msg,
                  icon:'none',
                  duration:3000
                })
            }
          }
        })
      }
    }
    ,
    goRegistPage(){
      wx.navigateTo({
        url: '../userRegist/regist',
      })
    }
})