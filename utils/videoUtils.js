function uploadVideo() {
  var that = this;
  wx.chooseVideo({
    sourceType: ['album'],
    success(res) {
      var duration = res.duration;
      var tmpWidth = res.width;
      var tmpHeight = res.height;
      var tmpVideoUrl = res.tempFilePath;
      var tmpCoverUrl = res.thumbTempFilePath;
      if (duration > 31) {
        wx.showToast({
          title: '视频长度不能超过30秒...',
          icon: "none",
          duration: 2500
        });
      } else if (duration < 1) {
        wx.showToast({
          title: '视频长度太短，请上传超过1秒的视频...',
          icon: "none",
          duration: 2500
        });
      } else {
        //  打开选择BGM的页面
        wx.navigateTo({
          url: "../chooseBgm/chooseBgm?duration=" + duration +
            "&tmpWidth=" + tmpWidth +
            "&tmpHeight=" + tmpHeight +
            "&tmpVideoUrl=" + tmpVideoUrl +
            "&tmpCoverUrl=" + tmpCoverUrl
        });
      }


    }
  })
}

module.exports ={
  uploadVideo: uploadVideo
}